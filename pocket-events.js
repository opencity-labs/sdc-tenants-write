const pocketbase = require('pocketbase/cjs')
const eventsource = require('eventsource')

global.EventSource = eventsource;




const start = async function() {
	const pb = new pocketbase('https://pocketbase.boat.opencontent.io');

	// list and filter "example" collection records
	const result = await pb.collection('sdc_tenants').getList(1, 20, {
    	filter: 'status = "active"'
	});
	const items = result.items;
	console.log(items);
}

start();

